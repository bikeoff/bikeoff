# bikeoff

A tool to help people use and find cycle parking.

## Some notes

Probably sensible to use Open Street Map as the Source of Truth.

ODI Leeds have a tree mapping tool: https://odileeds.org/projects/osmedit/trees/

It uses Open Street Map as an OAuth provider.
To login to the tree mapping tool,
you have to login to your Open Street Map account and
authorise the tool (creating an account first if necessary).

We could borrow that login flow.
It's probably using OAuth: https://wiki.openstreetmap.org/wiki/OAuth